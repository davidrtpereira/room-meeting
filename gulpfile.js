var gulp          = require('gulp');
var $             = require('gulp-load-plugins')();
var tildeImporter = require('node-sass-tilde-importer');
var browserSync   = require('browser-sync').create();

var webpack       = require('webpack');
var webpackConfig = require('./webpack.config.js')();
var bundler       = webpack(webpackConfig);

var isProduction  = !!$.util.env.production;

var paths = {
    svg: {
        src: 'resources/assets/img/svg/*.svg',
        dest: 'public/img'
    },
    icons: {
        src: 'resources/assets/img/icons/*.svg',
        dest: 'public/img'
    },
    images: {
        src: 'resources/assets/img/*.{jpg,png,gif}',
        dest: 'public/img'
    },
    sass: {
        src: 'resources/assets/sass/app.scss',
        dest: 'public/css'
    },
};

/**
 * SVG
 */
gulp.task('svg2png', function() {
    var svg = gulp.src(paths.svg.src);

    svg
        .pipe($.changed(paths.svg.dest, { extension: '.png' }))
        .pipe($.svg2png())
        .pipe($.imagemin())
        .pipe(gulp.dest(paths.svg.dest));

    svg
        .pipe($.changed(paths.svg.dest))
        .pipe($.imagemin())
        .pipe(gulp.dest(paths.svg.dest));

    return svg;
});


/**
 * Icons
 */
gulp.task('svgicons', function() {
    var path = require('path');

    return gulp.src(paths.icons.src)
        .pipe($.imagemin([
            $.imagemin.svgo(function(file) {
                var prefix = path.basename(file.relative, path.extname(file.relative));
                return {
                    plugins: [{
                        removeAttrs: { attrs : 'fill' },
                        cleanupIDs: {
                            prefix: prefix + '-',
                            minify: true
                        }
                    }]
                };
            })
        ]))
        .pipe($.svgstore())
        .pipe(gulp.dest(paths.icons.dest));
});


/**
 * Images
 */
gulp.task('images', function() {
    gulp.src(paths.images.src)
        .pipe($.imagemin())
        .pipe(gulp.dest(paths.images.dest));
});


/**
 * Sass
 */
gulp.task('sass', function() {
    return gulp.src(paths.sass.src)
        .pipe(isProduction ? $.util.noop() : $.sourcemaps.init())
        .pipe($.sass({
            importer: tildeImporter
        }).on('error', $.sass.logError))
        .pipe(isProduction ? $.autoprefixer(['last 2 versions', 'ie 8', 'ie 9', '> 1%']) : $.util.noop())
        .pipe(isProduction ? $.csso() : $.util.noop())
        .pipe(isProduction ? $.util.noop() : $.sourcemaps.write(''))
        .pipe(gulp.dest(paths.sass.dest))
        .pipe(browserSync.stream());
});


/**
 * JavaScript
 */
gulp.task('scripts', function(cb) {
    bundler.watch(200, function(err, stats) {
        if (err) {
            throw new $.util.PluginError('webpack', err);
        }

        $.util.log('[webpack]', stats.toString('minimal'));

        browserSync.reload(webpackConfig.output.filename);
    });

    return cb();
});

/**
 * Live preview
 */
gulp.task('serve', function() {
    browserSync.init({
        open: false,
        ghostMode: false,
        online: false,
        scrollRestoreTechnique: 'cookie',
        files: [
            '**/*.{html,php}',
            'public/img/*.{jpg,png,svg,gif,webp,ico}'
        ],
        proxy: 'reserve.dev.com'
    });
});


/**
 * Build
 */
gulp.task('build', ['sass', 'scripts', 'svg2png'], function() {
    $.util.log($.util.colors.green('Build is finished'));
});


/**
 * Watch files
 */
gulp.task('watch', ['serve'], function() {
    /* Watch styles */
    gulp.watch(['**/*.scss'], { cwd: 'resources/assets/sass' }, ['sass']);

    /* Watch SVG */
    gulp.watch(['*.svg'], { cwd: 'resources/assets/img/icons' }, ['svgicons']);
    gulp.watch(['*.svg'], { cwd: 'resources/assets/img/svg' },   ['svg2png']);

    /* Watch images */
    gulp.watch(['*.{jpg,png,gif}'], { cwd: 'resources/assets/img' }, ['images']);
});


/**
 * Default task
 */
gulp.task('default', ['watch', 'build']);