<?php
/**
 * Temporario
 */
Route::get('/', function (){
    return redirect()->route('auth.login');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('auth.show');
Route::post('login', 'Auth\LoginController@login')->name('auth.login');
Route::get('logout', 'Auth\LoginController@logout')->name('auth.logout');

Route::group(['prefix' => 'area-restrita', 'middleware' => ['auth']], function (){
    Route::get('home', 'RestrictedArea\HomeController@index')->name('restricted-area.index');
    /**
     * Rotas Associadas as usuários
     */
    Route::group(['prefix' => 'usuario'], function (){
        Route::get('', 'RestrictedArea\UserController@index')->name('restricted-area.user.index');
        Route::get('criar', 'RestrictedArea\UserController@create')->name('restricted-area.user.create');
        Route::post('store', 'RestrictedArea\UserController@store')->name('restricted-area.user.store');
        Route::group(['prefix' => '{userId}'], function (){
            Route::get('editar', 'RestrictedArea\UserController@edit')->name('restricted-area.user.edit');
            Route::put('atualizar', 'RestrictedArea\UserController@update')->name('restricted-area.user.update');
            Route::delete('excluir', 'RestrictedArea\UserController@destroy')->name('restricted-area.user.destroy');
        });
    });
    /**
     * Rotas Associadas as salas
     */
    Route::group(['prefix' => 'sala'], function (){
        Route::get('', 'RestrictedArea\RoomController@index')->name('restricted-area.room.index');
        Route::get('criar', 'RestrictedArea\RoomController@create')->name('restricted-area.room.create');
        Route::post('store', 'RestrictedArea\RoomController@store')->name('restricted-area.room.store');
        Route::group(['prefix' => '{userId}'], function (){
            Route::get('editar', 'RestrictedArea\RoomController@edit')->name('restricted-area.room.edit');
            Route::put('atualizar', 'RestrictedArea\RoomController@update')->name('restricted-area.room.update');
            Route::delete('excluir', 'RestrictedArea\RoomController@destroy')->name('restricted-area.room.destroy');
        });
    });

    /**
     * Rotas Associadas as reservas
     */
    Route::group(['prefix' => 'reserva'], function (){
        Route::get('', 'RestrictedArea\ReserveController@index')->name('restricted-area.reserve.index');
        Route::post('listar', 'RestrictedArea\ReserveController@lists')->name('restricted-area.reserve.lists');
        Route::post('salas', 'RestrictedArea\ReserveController@getRooms')->name('restricted-area.reserve.getRooms');
        Route::post('salvar', 'RestrictedArea\ReserveController@store')->name('restricted-area.reserve.store');
        Route::post('cancelar', 'RestrictedArea\ReserveController@destroy')->name('restricted-area.reserve.destroy');
    });

});