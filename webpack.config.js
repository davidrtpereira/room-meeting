var webpack = require('webpack');
var path = require('path');

module.exports = function (env) {
    return {
        entry: {
            main: './resources/assets/js/app.js',
        },
        output: {
            path: path.join(__dirname, 'public/js'),
            filename: '[name].js',
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js',
            },
        },
        externals: {
            'jquery': 'jQuery',
        },
        cache: true,
        devtool: (env === 'production') ? false : 'eval',
        plugins: [
            new webpack.ProvidePlugin({
                '$': 'jquery',
                'jQuery': 'jquery',
                'window.jQuery': 'jquery',
            }),
        ],
        module: {
            rules: [{
                test: /\.js$/,
                exclude: /node_modules|bower_components|vendor/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['env'],
                    },
                },
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            }],
        },
    };
};
