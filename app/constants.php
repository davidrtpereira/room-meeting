<?php

/**
 * General
 */
define('COLOR_RESERVE_FREE', '#86f442');
define('COLOR_RESERVE_BUSY', '#d12323');
define('HOUR_START', 0); // Meia noite
define('HOUR_END', 1380); // 23h
define('INTERVAL_RESERVES', 60); // 1 hora

/**
 * Users
 */
define('USER_TYPE_ADMIN', '1');
define('USER_TYPE_NORMAL', '2');

/**
 * Navegação
 */
define('SECTION_MENU', 'menu');
define('MENU_HOME', 'home');
define('MENU_USER', 'user');
define('MENU_ROOM', 'room');
define('MENU_RESERVE', 'reserve');

/**
 * Reserves
 */
define('RESERVE_STATUS_FREE', 1);
define('RESERVE_STATUS_BUSY', 2);