<?php

namespace App\Http\Requests\RestrictedArea;

use Illuminate\Foundation\Http\FormRequest;

class ReserveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_time' => ['required'],
            'room_id'   => ['required', 'exists:meeting_rooms,id']
        ];
    }
}
