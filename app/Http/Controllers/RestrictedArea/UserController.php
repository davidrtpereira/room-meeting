<?php

namespace App\Http\Controllers\RestrictedArea;

use App\Http\Requests\RestrictedArea\UserRequest;
use App\Http\Requests\RestrictedArea\UserUpdateRequest;
use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{

    protected $repository;

    public function __construct()
    {
        $this->repository = new UserRepository();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setMenu(MENU_USER);
        $users = $this->repository->getUsersPaginate();
        return view('restricted-area.user.index', compact('users'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $this->setMenu(MENU_USER);
        return view('restricted-area.user.create');
    }

    /**
     * @param UserRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request)
    {
        $this->repository->storeUser($request);
        return redirect()->route('restricted-area.user.index')->with(['message' => 'Usuário criado com sucesso.']);
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($userId)
    {
        $this->setMenu(MENU_USER);
        $user = $this->repository->getUserById($userId);
        if(!$user){
            throw new ModelNotFoundException('Usuário não encontrado');
        }
        return view('restricted-area.user.show', compact('user'));
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($userId)
    {
        $this->setMenu(MENU_USER);
        $user = $this->repository->getUserById($userId);
        if(!$user){
            throw new ModelNotFoundException('Usuário não encontrado');
        }
        return view('restricted-area.user.edit', compact('user'));
    }

    /**
     * @param UserUpdateRequest $request
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request, $userId)
    {
        $this->repository->updateUser($userId, $request);
        return redirect()->route('restricted-area.user.edit', $userId)->with(['message' => 'Usuário atualizado com sucesso.']);
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($userId)
    {
        $this->repository->deleteUser($userId);
        return redirect()->route('restricted-area.user.index')->with(['message' => 'Usuário Excluído com sucesso.']);
    }
}