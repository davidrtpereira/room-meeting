<?php

namespace App\Http\Controllers\RestrictedArea;

use App\Http\Requests\RestrictedArea\ReserveRequest;
use App\Repositories\ReserveRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReserveController extends Controller
{
    /**
     * @var ReserveRepository
     */
    protected $repository;

    /**
     * ReserveController constructor.
     */
    public function __construct()
    {
        $this->repository = new ReserveRepository();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setMenu(MENU_RESERVE);
        return view('restricted-area.reserve.index');
    }

    /**
     * @param ReserveRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReserveRequest $request)
    {
        $reserve = $this->repository->reserveRoom($request->date_time, $request->room_id);
        return response()->json(compact('reserve'), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $this->repository->reserveCancel($request->reserve_id);
        return response()->json([], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function lists(Request $request)
    {
        $reserves = $this->repository->listHoursFreeAndBusy($request->start, $request->end);
        return response()->json(compact('reserves'), 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRooms(Request $request)
    {
        $rooms = $this->repository->getRoomsAvailable($request->date_time);
        return response()->json(compact('rooms'));
    }
}