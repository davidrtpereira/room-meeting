<?php

namespace App\Http\Controllers\RestrictedArea;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->setMenu(MENU_HOME);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setMenu(MENU_HOME);
        return view('restricted-area.index');
    }
}
