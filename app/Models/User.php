<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /****** Mutators *****/

    /**
     * @param $password
     */
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    /****** Scopes *****/

    /**
     * @param $query
     * @param bool $isAdmin
     */
    public function scopeIsAdmin($query, $isAdmin = true)
    {
        if($isAdmin){
            $query->where('type', USER_TYPE_ADMIN);
        }else{
            $query->where('type', USER_TYPE_NORMAL);
        }
    }
}
