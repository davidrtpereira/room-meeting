<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 02/06/18
 * Time: 17:47
 */

namespace App\Repositories;


use App\Models\User;

class UserRepository
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * @return mixed
     */
    public function getUsersPaginate()
    {
        return $this->model->isAdmin(false)->paginate();
    }

    /**
     * @param int $userId
     * @return \App\Models\User
     */
    public function getUserById($userId)
    {
        return $this->model->find($userId);
    }

    /**
     * @param \App\Http\Requests\RestrictedArea\UserRequest $request
     * @return \App\Models\User
     */
    public function storeUser($request)
    {
        $request->request->add(['type' => USER_TYPE_NORMAL]);
        $userStored = $this->model->create($request->all());
        return $userStored;
    }

    /**
     * @param int $userId
     * @param \App\Http\Requests\RestrictedArea\UserUpdateRequest $request
     * @return boolean
     */
    public function updateUser($userId, $request)
    {
        $userUpdated = $this->model->where('id', $userId)->update($request->only(['name', 'email']));
        return $userUpdated;
    }

    /**
     * @param int $userId
     * @return boolean
     */
    public function deleteUser($userId)
    {
        $userDelete = $this->model->where('id', $userId)->delete();
        return $userDelete;
    }
}