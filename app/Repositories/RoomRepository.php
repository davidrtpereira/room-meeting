<?php
/**
 * Created by PhpStorm.
 * User: david
 * Date: 02/06/18
 * Time: 17:47
 */

namespace App\Repositories;


use App\Models\MeetingRoom;
use App\Models\User;

class RoomRepository
{
    /**
     * @var User
     */
    protected $model;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new MeetingRoom();
    }

    /**
     * @return mixed
     */
    public function getRoomsPaginate()
    {
        return $this->model->paginate();
    }

    /**
     * @param int $roomId
     * @return \App\Models\MeetingRoom
     */
    public function getRoomById($roomId)
    {
        return $this->model->find($roomId);
    }

    /**
     * @param \App\Http\Requests\RestrictedArea\RoomRequest $request
     * @return \App\Models\MeetingRoom
     */
    public function storeRoom($request)
    {
        $room = $this->model->create($request->all());
        return $room;
    }

    /**
     * @param int $roomId
     * @param \App\Http\Requests\RestrictedArea\RoomUpdateRequest $request
     * @return boolean
     */
    public function updateRoom($roomId, $request)
    {
        $roomUpdated = $this->model->where('id', $roomId)->update($request->only(['name']));
        return $roomUpdated;
    }

    /**
     * @param int $roomId
     * @return boolean
     */
    public function deleteRoom($roomId)
    {
        $userDelete = $this->model->where('id', $roomId)->delete();
        return $userDelete;
    }
}