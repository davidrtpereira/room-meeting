<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'          => 'Administrador',
            'email'         => 'admin@teste.com',
            'password'      => bcrypt('admin'),
            'type'          => USER_TYPE_ADMIN,
            'created_at'    => DB::raw('NOW()'),
            'updated_at'    => DB::raw('NOW()'),
        ]);
    }
}
