<head>
    <title>{{config('app.name')}}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Reserva de salas de Reunião">
    <meta name="title" content="{{config('app.name')}}">
    <meta name="csrf-token" content="{{csrf_token()}}">
    @include('partials.styles')
</head>