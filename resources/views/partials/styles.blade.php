<link rel="stylesheet" href="{{asset('css/app.css')}}">
<link rel="icon" type="image/png" href="{{asset('favicon.png')}}">
@yield('styles')