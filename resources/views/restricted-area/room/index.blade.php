@extends('restricted-area.partials.base')
@section('content')
    <div class="container room">
        <h2>Salas</h2>
        @include('partials.messages')
        <a href="{{route('restricted-area.room.create')}}" class="btn btn-success">Criar</a>
        @include('restricted-area.room.list')
    </div>
@endsection