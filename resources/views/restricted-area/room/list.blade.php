<table class="table room-list">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nome</th>
            <th scope="col">Ações</th>
        </tr>
    </thead>
    <tbody>
        @forelse($rooms as $room)
            <tr>
                <th scope="row">{{$room->id}}</th>
                <td>{{$room->name}}</td>
                <td>
                    <a href="{{route('restricted-area.room.edit', $room->id)}}" class="btn btn-primary">Editar</a>
                    {!! Form::open(['route' => ['restricted-area.room.destroy', $room->id], 'method' => 'DELETE']) !!}
                    <button class="btn btn-danger -btn-delete">Excluir</button>
                    {!! Form::close() !!}
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="3" class="text-center"> Nenhuma sala cadastrada </td>
            </tr>
        @endforelse
    </tbody>
</table>
{{$rooms->render()}}