@extends('restricted-area.partials.base')
@section('content')
    <div class="container">
        <h2>Editar {{$room->name}}</h2>
        <div class="form-user">
            @include('partials.messages')
            {!! Form::model($room, ['route' => ['restricted-area.room.update', $room->id], 'method' => 'PUT']) !!}
                @include('restricted-area.room.form')
                <div class="form-action">
                    <button class="btn btn-primary">Atualizar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection