@extends('restricted-area.partials.base')
@section('content')
    <div class="container">
        <h2>Criar Sala</h2>
        <div class="form-user">
            @include('partials.messages')
            <form action="{{route('restricted-area.room.store')}}" method="POST">
                {{csrf_field()}}
                @include('restricted-area.room.form')
                <div class="form-action">
                    <button class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
@endsection