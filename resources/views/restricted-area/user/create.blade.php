@extends('restricted-area.partials.base')
@section('content')
    <div class="container">
        <h2>Criar usuário</h2>
        <div class="form-user">
            @include('partials.messages')
            <form action="{{route('restricted-area.user.store')}}" method="POST">
                {{csrf_field()}}
                @include('restricted-area.user.form')
                <div class="form-action">
                    <button class="btn btn-primary">Salvar</button>
                </div>
            </form>
        </div>
    </div>
@endsection