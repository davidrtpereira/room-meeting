@extends('restricted-area.partials.base')
@section('content')
    <div class="container">
        <h2>Editar {{$user->name}}</h2>
        <div class="form-user">
            @include('partials.messages')
            {!! Form::model($user, ['route' => ['restricted-area.user.update', $user->id], 'method' => 'PUT']) !!}
                @include('restricted-area.user.form')
                <div class="form-action">
                    <button class="btn btn-primary">Atualizar</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection