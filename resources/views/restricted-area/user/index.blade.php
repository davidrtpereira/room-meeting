@extends('restricted-area.partials.base')
@section('content')
    <div class="container user">
        <h2>Usuários</h2>
        @include('partials.messages')
        <a href="{{route('restricted-area.user.create')}}" class="btn btn-success">Criar</a>
        @include('restricted-area.user.list')
    </div>
@endsection