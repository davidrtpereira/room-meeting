<div class="form-group">
    <label for="name">Nome</label>
    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nome do usuário']) !!}
</div>
<div class="form-group">
    <label for="email">E-mail</label>
    {!! Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'email@teste.com']) !!}
</div>
@if(!isset($user))
    <div class="form-group">
        <label for="password">Senha</label>
        {!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'placeholder' => '*****']) !!}
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirmar Senha</label>
        {!! Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'password_confirmation', 'placeholder' => '*****']) !!}
    </div>
@endif