<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="#">Reserva de Salas</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="{{session()->get(SECTION_MENU) == MENU_HOME ? 'active' : ''}}"><a href="{{route('restricted-area.index')}}">Home</a></li>
            <li class="{{session()->get(SECTION_MENU) == MENU_USER ? 'active' : ''}}"><a href="{{route('restricted-area.user.index')}}">Usuários</a></li>
            <li class="{{session()->get(SECTION_MENU) == MENU_ROOM ? 'active' : ''}}"><a href="{{route('restricted-area.room.index')}}">Salas</a></li>
            <li class="{{session()->get(SECTION_MENU) == MENU_RESERVE ? 'active' : ''}}"><a href="{{route('restricted-area.reserve.index')}}">Reservas</a></li>
            <li class=""><a href="{{route('auth.logout')}}"><small>logout</small></a></li>
        </ul>
    </div>
</nav>