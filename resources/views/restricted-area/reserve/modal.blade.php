<b-modal ref="modalReserve" hide-footer id="modalReserve" title="Reservar Sala">
    <p class="text-center">Escolha sua sala!</p>
    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label for="">Horário</label>
                <input type="text" class="form-control" disabled :v-model="selected.date_time" :value="selected.date_time">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label for="">Sala</label>
                <select name="roms" id="rooms" class="form-control" :disabled="!(rooms.length > 0)" v-model="selected.room_id">
                    <option :value="null" selected disabled>Selecione um sala</option>
                    <option :value="room.id" v-for="(room, key) in rooms">@{{ room.name}}</option>
                </select>
            </div>
        </div>
    </div>
    <button type="button" class="btn btn-default" @click="closeModal">Fechar</button>
    <button type="button" :disabled="selected.room_id == null" class="btn btn-success" @click="saveReserve">Reservar</button>
</b-modal>
<b-modal ref="modalReserved" hide-footer id="modalReserved" title="Reserva de Sala">
    <p class="text-center">Sala reservada</p>
    <div v-if="selected.type == {{RESERVE_STATUS_BUSY}}">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="">Usuário</label>
                    <input type="text" class="form-control" disabled :value="selected.reserve.user.name">
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="">Horário</label>
                    <input type="text" class="form-control" disabled :value="selected.reserve.date_time">
                </div>
            </div>
            <div class="col-md-8">
                <div class="form-group">
                    <label for="">Sala</label>
                    <input type="text" class="form-control" disabled :value="selected.reserve.meeting_room.name">
                </div>
            </div>
        </div>
        <button type="button" class="btn btn-default" @click="closeModalReserved">Fechar</button>
        <button type="button" class="btn btn-danger" v-if="!selected.past && selected.reserve.user_id == {{auth()->id()}}" @click="cancelReserve">Cancelar Reserva</button>
    </div>
</b-modal>