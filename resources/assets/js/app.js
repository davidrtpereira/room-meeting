import Vue from 'vue';
require('./bootstrap');

// Components
import TheReserve from './components/TheReserve.vue';
import FullCalendar from 'vue-full-calendar';
import bModal from 'bootstrap-vue/es/components/modal/modal'

Vue.use(FullCalendar);

const app = new Vue({
    el: '#app',
    components: {
        TheReserve,
        bModal
    }
});

export default app;
